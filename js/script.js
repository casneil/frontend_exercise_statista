const fizzBuzzContainer = document.getElementById("fizzBuzzList");
const fizzBuzzUl = document.getElementById("fizzBuzzUl");
const generateBtn = document.getElementById("btn--generate");
const closeBtn = document.getElementById("btn--close");

fizzBuzzContainer.appendChild(fizzBuzzUl);

const generateFizzBuzz = () => {
  for (let i = 1; i <= 100; i++) {
    const li = document.createElement("li");
    fizzBuzzUl.appendChild(li);
    if (i % 15 === 0) {
      li.className = "fizz buzz";
      li.innerHTML = "FizzBuzz";
    } else if (i % 3 === 0) {
      li.className = "fizz";
      li.innerHTML = "Fizz";
    } else if (i % 5 === 0) {
      li.className = "buzz";
      li.innerHTML = "Buzz";
    } else {
      li.className = "fizz--num";
      li.innerHTML = i;
    }
  }
};

generateBtn.addEventListener("click", () => {
  generateBtn.style.display = "none";
  closeBtn.style.display = "block";
  generateFizzBuzz();
});

closeBtn.addEventListener("click", () => {
  closeBtn.style.display = "none";
  fizzBuzzUl.innerHTML = "";
  generateBtn.style.display = "block";
});
