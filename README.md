# Frontend Exercise Statista

The aim of this exercise was to:

1. Checking the provided templates for design and programming errors and improve the overall quality
2. Display a FizzBuzz list

---

## Features

- Toggle button for generating FizzBuzz
- Fizz Buzz FizzBuzz and number have unique styling
- Responsive
- Matches the coporate design of Statista

---

#### Screenshots

![picture](screenshots/preview.gif)
![picture](screenshots/lighthouse.png)
